### Run Container Docker Manually

```bash
docker build -t zweicom-simple-server .
docker run --rm --name zweicom-simple-server-app -d -p 5000:5000 zweicom-simple-server
```

### Run Container with Compose for local Deploy

```bash
docker-compose up -d
```

### Documentation for test Deployment

The test deployment was testing in Pipeline GitLab.
The file with jos is .gitlab-ci.yml

Test and Deployment was successfuly

![NodeJs](./img/pipelines.png)