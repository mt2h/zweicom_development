FROM python:3.7-slim

WORKDIR /app

COPY . .

RUN pip install --upgrade pip && pip install -r requirements.txt

CMD ["python", "service.py"]

EXPOSE 5000/tcp
